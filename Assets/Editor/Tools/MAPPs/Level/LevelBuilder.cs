﻿

/*
*      @Author: Carlos Miguel Aquino
*      @Copyright: (c) 2019 All Rights Reserved
*      @Company: VFS ROBORAPTURE
*      @Contact: maquinodesign@gmail.com | pg15miguel@vfs.com
*/

using System.Collections;
using System.Collections.Generic;
using Edu.Vfs.RoboRapture.ScriptableLibrary;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace Edu.Vfs.RoboRapture.Mapps
{    
    ///<summary>
    ///-summary of script here-
    ///</summary>
    [ExecuteInEditMode]
    public class LevelBuilder : EditorWindow
    {
        private GameObject Level;
        private GameObject TileTemplate;

        private Vector2Int LevelSize;
        private List<GameObject> TileList = new List<GameObject>();

        [MenuItem("MAPPs/Levels %t")]
        public static void ExportToLevelPrefab()
        {
            EditorWindow.GetWindow(typeof(LevelBuilder));
        }

        private void GenerateBlocks(GameObject tileTemplate)
        {
            if(TileList.Count > 0)
            {
                foreach (var item in TileList)
                {
                    DestroyImmediate(item);   
                }
            }

            for (int i = 0; i < LevelSize.x; i++)
            {
                for (int k = 0; k < LevelSize.y; k++)
                {
                    GameObject go = PrefabUtility.InstantiatePrefab(tileTemplate as GameObject) as GameObject;
                    
                    Tile tile = go.GetComponent<Tile>();
                    tile.EntityPosition.x = i;
                    tile.EntityPosition.z = k;

                    go.transform.parent = Level.transform;
                    TileList.Add(go);
                }
            }

            GameObject entCont = new GameObject();

            entCont.AddComponent(typeof(EntityParser));
            Level.AddComponent(typeof(Board));
            Level.AddComponent(typeof(OnTileLoad));
            Level.GetComponent<Board>().BoardDimensions = new Vector3Int(LevelSize.x, 0 , LevelSize.y);
            entCont.transform.parent = Level.transform;
            entCont.name = "EntityContainer";
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("LEVEL EXPORTER ");

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Level To Be Exported: ");
            Level = (GameObject)EditorGUILayout.ObjectField(Level, typeof(GameObject), true);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Tile Template: ");
            TileTemplate = (GameObject)EditorGUILayout.ObjectField(TileTemplate, typeof(GameObject), true);

            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(" Grid Size: ");
            LevelSize = EditorGUILayout.Vector2IntField("", LevelSize);

            EditorGUILayout.EndHorizontal();

            if(GUILayout.Button("Generate Grid", GUILayout.ExpandWidth(false)))
            {
                if ( TileTemplate == null)
                { 
                    Debug.LogError("There's no Tile Template!");
                    return;
                }

                if(Level == null || Level.name.Contains("_Level") == false){Debug.LogError("No _Level Object Found"); return;}
                GenerateBlocks(TileTemplate);
            }
            
            CreateExportButton();
        }

        private void CreateExportButton()
        {
            if(GUILayout.Button("Export As Prefab", GUILayout.ExpandWidth(false)))
            {
                if(Level != null)
                {
                    if(Level.name.Contains("_Level"))
                    {
                        var prefab = PrefabUtility.SaveAsPrefabAsset(Level, $"Assets/Prefabs/Levels/{Level.name}.prefab", out var success);
                        if(success)
                        {
                            Debug.Log($"Created {Level.name}.prefab inside of  ' Assets/Prefabs/Levels '  ");
                        }
                    }
                    else
                    {
                        Debug.LogError("Gameobject selected for export is not named with _Level");
                    }
                }
                else
                {
                    Debug.LogError("There's no gameobject selected for export.");
                }
            }
        }
    }
}
#endif