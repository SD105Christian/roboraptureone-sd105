﻿/**
*
*   @ Author: Carlos Miguel Aquino
*   @ Copyright: (c) All Rights Reserved
*   @ Contact Info: pg15miguel@vfs.com || maquinodesgin@gmail.com
*
*   All Scripts are made for their respected projects. Contact the author for more information
*   
*/
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Edu.Vfs.RoboRapture.TerrainSystem;
using Edu.Vfs.RoboRapture.Environment;

#if UNITY_EDITOR
namespace Edu.Vfs.RoboRapture.Mapps
{
    public enum TerrainVariants
    {
        GLACIER,
        QUAGMIRE,
        ORCHARD,
        RAPTURE
    }

    public enum TileVariant
    {
        ORIGINAL,
        ALTERNATE,
        MOUNTAINBASE,
        SPECIAL
    }

    [ExecuteInEditMode]
    public class BlockEditor : EditorWindow
    {
        private  TileDataCollection Collection;
        private GameObject[] _selectedBlocks;

        private Dictionary<TerrainVariants, TileData[]> TileDataDictionary;

        // Tile Description
        private string TileName;
        private Vector3 TilePos;
        private Sprite TileImage;
        private string TileDescription;
        private TerrainVariants TerrainVariant;
        private TileVariant tileVariant;

        // Tile Terrain
        private TileType TerrainType;
        private TerrainNavigationType navigationType;

        // Tile Decorator
        private ATileDecoration Decoration;

        [MenuItem("MAPPs/BLock %e")]
        public static void OpenWindow()
        {
            EditorWindow.GetWindow(typeof(BlockEditor));
        }

        private bool ValidateTile(GameObject obj)
        {
            Tile tile = obj.GetComponent<Tile>();
            RoboRaptureTerrain terrain = obj.GetComponent<RoboRaptureTerrain>();
            TileDecorator decorator = obj.GetComponent<TileDecorator>();
            TileHovered hovered = obj.GetComponent<TileHovered>();

            if(tile == null)
            {
                Debug.LogError($"{obj.name} is missing Tile Script", obj);
                return false;
            }

            if(terrain == null)
            {
                Debug.LogError($"{obj.name} is missing RoboRaptureTerrain Script", obj);
                return false;
            }

            if(decorator == null)
            {
                Debug.LogError($"{obj.name} is missing Tile Decorator Script", obj);
                return false;
            }

            if(hovered == null)
            {
                Debug.LogError($"{obj.name} is missing Tile Hovered Script", obj);
                return false;
            }

            return true;
        }

        private bool PreviewFirstSelected(GameObject obj)
        {
            if(ValidateTile(obj) == false)
            {
                // The tile is not valid.
                Debug.LogError($"{obj.name} is not a tile or is missing tile scripts. make sure that the tile has all of the scripts needed!", obj);
                return false;
            }

            Tile tile = obj.GetComponent<Tile>();
            RoboRaptureTerrain terrain = obj.GetComponent<RoboRaptureTerrain>();
            TileDecorator decorator = obj.GetComponent<TileDecorator>();

            TileName = tile.TileName;
            TileImage = tile.Image;
            TileDescription = tile.Description;

            TerrainType = terrain.TerrainEffect;
            navigationType = terrain.NavigationType;

            return true;
        }

        private bool EvaluateTileDescription(GameObject obj)
        {
            try
            {
                Tile tile = obj.GetComponent<Tile>();

                Undo.RecordObject(tile, "Description Update");

                obj.name = TileName;
                tile.TileName = TileName;
                tile.Image = TileImage;
                tile.Description = TileDescription;

                PrefabUtility.RecordPrefabInstancePropertyModifications(tile);
                
                // TODO apply material of the tile.
                TileComponent[] tileComponents = obj.GetComponentsInChildren<TileComponent>();

                if(tileComponents.Length <= 0)
                {
                    return true;
                }

                foreach (TileComponent component in tileComponents)
                {
                    Undo.RecordObject(component.gameObject, "Material Change");
                    if(component.name.Contains("Top"))
                    {
                        foreach (var item in TileDataDictionary[TerrainVariant])
                        {
                            if(item.terrainType == tileVariant)
                            {
                                component.GetComponent<MeshRenderer>().material = item.Top;
                            }
                        }
                    }

                    if(component.name.Contains("Body"))
                    {
                        foreach (var item in TileDataDictionary[TerrainVariant])
                        {
                            if(item.terrainType == tileVariant)
                            {
                                component.GetComponent<MeshRenderer>().material = item.Body;
                            }
                        }
                    }
                    PrefabUtility.RecordPrefabInstancePropertyModifications(component.gameObject);
                }
                return true;
            }
            catch(System.Exception)
            {
                Debug.LogError("Error Setting Tile Description");
                return false;
            }
        }

        private bool EvaluateTileTerrain(GameObject obj)
        {
            try
            {
                RoboRaptureTerrain terrain = obj.GetComponent<RoboRaptureTerrain>();
                terrain.TerrainEffect = TerrainType;
                terrain.NavigationType = navigationType;
                return true;
            }
            catch(System.Exception)
            {
                Debug.LogError("Error Setting Terrain Description");
                return false;
            }
        }

        private bool EvaluateDecoration(GameObject obj)
        {
            try
            {
                TileDecorator decor = obj.GetComponent<TileDecorator>();
                decor.SetDecorator(Decoration);
                return true;
            }
            catch(System.Exception)
            {
                Debug.LogError("Error Setting Decoration");
                return false;
            }
        }

        private void Awake()
        {
            Selection.selectionChanged += ChangedTile;
        }

        public void ChangedTile()
        {

        }
        
        private void OnGUI()
        {
            if(Collection != null)
            { 
                TileDataDictionary = Collection.GetDictionary();
                _selectedBlocks = (GameObject[])Selection.gameObjects;
                Debug.Log($"Changed Tile to{_selectedBlocks}");
                
                // PreviewFirstSelected(_selectedBlocks[0]);

                EditorGUILayout.LabelField("BLOCK EDITOR");

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("Tile Details");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Position");
                //  need a v3 layout that's read only
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Tile Image");
                TileImage = (Sprite)EditorGUILayout.ObjectField(TileImage, typeof(Sprite), true);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Tile Name");
                TileName = EditorGUILayout.TextField(TileName);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Tile Description");
                TileDescription = EditorGUILayout.TextArea(TileDescription);                    
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Terrain Variant");
                TerrainVariant = (TerrainVariants)EditorGUILayout.EnumPopup(TerrainVariant);
                EditorGUILayout.EndHorizontal();
                
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Tile Variant");
                tileVariant = (TileVariant)EditorGUILayout.EnumPopup(tileVariant);
                EditorGUILayout.EndHorizontal();
                
                EditorGUILayout.Space();

                EditorGUILayout.LabelField("Terrain");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Terrain Type");
                TerrainType = (TileType)EditorGUILayout.EnumPopup(TerrainType);
                EditorGUILayout.EndHorizontal();


                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Terrain Navigation Type");
                //TODO: make this pick from navigation type.
                navigationType = (TerrainNavigationType)EditorGUILayout.EnumPopup(navigationType);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("Decorator");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Decoration");
                Decoration = (ATileDecoration)EditorGUILayout.ObjectField(Decoration, typeof(ATileDecoration), true);
                EditorGUILayout.EndHorizontal();

                GUILayout.Space(10);
                
                if(GUILayout.Button("Extract Partial Tile Info"))
                {
                    PreviewFirstSelected(_selectedBlocks[0]);
                }

                GUILayout.Space(40);

                if(GUILayout.Button("Apply Specs To Tile"))
                {
                    foreach (GameObject block in _selectedBlocks)
                    {
                        if(ValidateTile(block) == false)
                        {
                            continue;
                        }                  

                        EvaluateTileDescription(block);
                        EvaluateTileTerrain(block);
                        EvaluateDecoration(block);
                    }
                }

                
            }
            
            else
            {
                Collection = (TileDataCollection)EditorGUILayout.ObjectField(Collection, typeof(TileDataCollection), true);
                return;
            }
        }
    }
}
#endif