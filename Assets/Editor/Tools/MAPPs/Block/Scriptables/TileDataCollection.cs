﻿

/*
*      @Copyright: (c) 2019 All Rights Reserved
*      @Company: VFS ROBORAPTURE
*      @Contact: maquinodesign@gmail.com | pg15miguel@vfs.com
*      @Author: Carlos Miguel Aquino
*/

using System.Collections;
using System.Collections.Generic;
using Edu.Vfs.RoboRapture.TerrainSystem;
using UnityEngine;

namespace Edu.Vfs.RoboRapture.Mapps
{    
    ///<summary>
    ///-summary of script here-
    ///</summary>
    [CreateAssetMenu(menuName="MAPPs/CollectionData")]
    public class TileDataCollection : ScriptableObject
    {
        [SerializeField]
        TileDataType[] TileDatas;

        public Dictionary<TerrainVariants, TileData[]> GetDictionary()
        {
            Dictionary<TerrainVariants, TileData[]> tempDictionary = new Dictionary<TerrainVariants, TileData[]>();

            foreach (var item in TileDatas)
            {
                tempDictionary[item.Type] = item.Data;
            }

            return tempDictionary;
        }
    }
}