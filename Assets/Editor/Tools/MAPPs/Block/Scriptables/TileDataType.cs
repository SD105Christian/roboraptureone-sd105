﻿

/*
*      @Copyright: (c) 2019 All Rights Reserved
*      @Company: VFS ROBORAPTURE
*      @Contact: maquinodesign@gmail.com | pg15miguel@vfs.com
*      @Author: Carlos Miguel Aquino
*/

using System.Collections;
using System.Collections.Generic;
using Edu.Vfs.RoboRapture.TerrainSystem;
using UnityEngine;

///<summary>
///-summary of script here-
///</summary>

namespace Edu.Vfs.RoboRapture.Mapps
{    
    [System.Serializable]
    public struct TileDataType
    {
        public TileData[] Data;
        public TerrainVariants Type;
    }
}
