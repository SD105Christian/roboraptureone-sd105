/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ENEMY_MOPELESS_HIT = 4043823570U;
        static const AkUniqueID PLAY_ALESSA_DEATH = 3463576202U;
        static const AkUniqueID PLAY_ALESSA_FIRE = 3152760336U;
        static const AkUniqueID PLAY_ALESSA_HIT = 1821161051U;
        static const AkUniqueID PLAY_ALESSA_LASER = 2441153917U;
        static const AkUniqueID PLAY_AMB_BONEYARD = 1330969791U;
        static const AkUniqueID PLAY_AMB_BONEYARD_BREAKINGBONES = 444680530U;
        static const AkUniqueID PLAY_AMB_FINALBOSS = 2223142824U;
        static const AkUniqueID PLAY_AMB_GLACIER = 3605158744U;
        static const AkUniqueID PLAY_AMB_QUAGMIRE = 3798631166U;
        static const AkUniqueID PLAY_BRUTUS_DEATH = 598494170U;
        static const AkUniqueID PLAY_BRUTUS_HIT = 1170633963U;
        static const AkUniqueID PLAY_BRUTUS_PUNCHATTACK = 374158680U;
        static const AkUniqueID PLAY_BRUTUS_RAMATTACK = 62681488U;
        static const AkUniqueID PLAY_CLICKON_ALESSA = 3338477511U;
        static const AkUniqueID PLAY_CLICKON_BRUTUS = 1679785819U;
        static const AkUniqueID PLAY_CLICKON_INCARNATE = 447523515U;
        static const AkUniqueID PLAY_CLICKON_MAXIMILIAN = 120420809U;
        static const AkUniqueID PLAY_CLICKON_MOPELESS = 2396042750U;
        static const AkUniqueID PLAY_CLICKON_PHANTASM = 328771828U;
        static const AkUniqueID PLAY_CLICKON_TESTAMENT = 2082182705U;
        static const AkUniqueID PLAY_CLICKON_UDEUKEDEFRUKE = 2614886301U;
        static const AkUniqueID PLAY_CLICKON_WHELP = 1582809870U;
        static const AkUniqueID PLAY_CLICKON_ZIGGURAT = 728242119U;
        static const AkUniqueID PLAY_ENEMY_DEATH_COUNTER_CLICK = 1015029493U;
        static const AkUniqueID PLAY_ENEMY_DEATH_COUNTER_END = 2043197614U;
        static const AkUniqueID PLAY_ENEMY_EXP_SFX = 4019891586U;
        static const AkUniqueID PLAY_ENEMY_MOV = 2010613501U;
        static const AkUniqueID PLAY_ENEMY_SUMMON = 961730208U;
        static const AkUniqueID PLAY_GLACIER_MOUNTAIN_SLIDE = 492344359U;
        static const AkUniqueID PLAY_GLACIER_MOUNTAINS_DESTRUCTION = 3786581253U;
        static const AkUniqueID PLAY_HELLBLOOM_DEATH = 1189210579U;
        static const AkUniqueID PLAY_HELLBLOOM_SPAWN = 766822404U;
        static const AkUniqueID PLAY_HIT_ENEMY = 3889018732U;
        static const AkUniqueID PLAY_HIT_PLAYER = 2257378511U;
        static const AkUniqueID PLAY_INCARNATE_DEATH = 3130030946U;
        static const AkUniqueID PLAY_INCARNATE_HEALING = 227241686U;
        static const AkUniqueID PLAY_INCARNATE_HIT = 677738179U;
        static const AkUniqueID PLAY_INCARNATE_TRANSFORM = 2153335472U;
        static const AkUniqueID PLAY_MAXIMILIAN_DEATH = 3749828868U;
        static const AkUniqueID PLAY_MAXIMILIAN_HIT = 3048413413U;
        static const AkUniqueID PLAY_MAXIMILIAN_PHYSIC_ATTACK = 560992969U;
        static const AkUniqueID PLAY_MAXIMILIAN_SHOCK_ATTACK = 4256044283U;
        static const AkUniqueID PLAY_MOPELESS_ATTACK = 2342275283U;
        static const AkUniqueID PLAY_MOPELESS_DEATH = 2035899795U;
        static const AkUniqueID PLAY_MOPELESS_HIT = 1541332066U;
        static const AkUniqueID PLAY_MUX_BONEYARD = 3946041105U;
        static const AkUniqueID PLAY_MUX_CREDIT = 1694167904U;
        static const AkUniqueID PLAY_MUX_GLACIER = 1403156162U;
        static const AkUniqueID PLAY_MUX_GLACIER_PROTOTYPE = 1246076309U;
        static const AkUniqueID PLAY_MUX_LOSESCREEN = 1261309312U;
        static const AkUniqueID PLAY_MUX_MENU = 2932749482U;
        static const AkUniqueID PLAY_MUX_QUAGMIRE = 2911295864U;
        static const AkUniqueID PLAY_MUX_RAPTURE = 4276438574U;
        static const AkUniqueID PLAY_NEOSATAN_HEAD_IDLE = 3477074675U;
        static const AkUniqueID PLAY_NEOSATAN_HIT = 2651492635U;
        static const AkUniqueID PLAY_NEOSATAN_PUKE = 1604264381U;
        static const AkUniqueID PLAY_NEOSATAN_PUKEOBJECT = 1242719756U;
        static const AkUniqueID PLAY_NEOSATAN_ROARSHOCKWAVE = 305417217U;
        static const AkUniqueID PLAY_NEOSATAN_RUMBLE_CAMARASHAKE = 37207887U;
        static const AkUniqueID PLAY_NEOSATAN_STOMP = 125820355U;
        static const AkUniqueID PLAY_PHANTASM_DEAD = 2172841107U;
        static const AkUniqueID PLAY_PHANTASM_HIT = 3870346656U;
        static const AkUniqueID PLAY_PHANTASM_SUMMON = 3305633968U;
        static const AkUniqueID PLAY_QUAGMIRE_THUNDER = 1691497386U;
        static const AkUniqueID PLAY_TESTAMENT_ATTACK = 896671806U;
        static const AkUniqueID PLAY_TESTAMENT_DEAD = 3572615252U;
        static const AkUniqueID PLAY_TESTAMENT_HIT = 147215685U;
        static const AkUniqueID PLAY_TESTAMENT_IDLE = 2113854776U;
        static const AkUniqueID PLAY_TILE_SFX = 682806854U;
        static const AkUniqueID PLAY_UDEUKEDEFRUKE_DEAD = 2951790104U;
        static const AkUniqueID PLAY_UDEUKEDEFRUKE_HIT = 1156928193U;
        static const AkUniqueID PLAY_UDEUKEDEFRUKE_SPLIT = 4199339094U;
        static const AkUniqueID PLAY_UI_INGAME_ACTION = 3636095737U;
        static const AkUniqueID PLAY_UI_INGAME_BANNER = 1586582105U;
        static const AkUniqueID PLAY_UI_INGAME_BUYABILITY = 3303056113U;
        static const AkUniqueID PLAY_UI_INGAME_BUYABILITY_SFX = 1178420641U;
        static const AkUniqueID PLAY_UI_INGAME_CAMERAZOOM = 2357046435U;
        static const AkUniqueID PLAY_UI_INGAME_CLICKON_UNITSCREEN = 4121755263U;
        static const AkUniqueID PLAY_UI_INGAME_EXP_UP = 2378316884U;
        static const AkUniqueID PLAY_UI_INGAME_HIGHLIGHT = 3376637825U;
        static const AkUniqueID PLAY_UI_INGAME_LOCKABILITY = 3486942012U;
        static const AkUniqueID PLAY_UI_INGAME_PLACE_UNIT = 3539079917U;
        static const AkUniqueID PLAY_UI_INGAME_SUBACTION = 3703367945U;
        static const AkUniqueID PLAY_UI_MENU_BACK = 777803296U;
        static const AkUniqueID PLAY_UI_MENU_CURSORMOVEMENT = 951065000U;
        static const AkUniqueID PLAY_UI_MENU_SELECTION = 2157261217U;
        static const AkUniqueID PLAY_UNIT_MOV = 4239036235U;
        static const AkUniqueID PLAY_VO_INCARNATE_LAUGH = 2830198671U;
        static const AkUniqueID PLAY_VO_MOPELESS_IDLE = 2233547917U;
        static const AkUniqueID PLAY_VO_NEOSATAN_DEATH = 3024806074U;
        static const AkUniqueID PLAY_VO_NEOSATAN_GROWL = 1519327747U;
        static const AkUniqueID PLAY_VO_TESTAMENT_IDLE = 1683512840U;
        static const AkUniqueID PLAY_VO_UDEUKEDEFRUKE_IDLE = 2698740588U;
        static const AkUniqueID PLAY_VO_WHELP_IDLE = 183598297U;
        static const AkUniqueID PLAY_VO_ZIGGURAT_IDLE = 4210359914U;
        static const AkUniqueID PLAY_WHELP_ATTACK = 3048452903U;
        static const AkUniqueID PLAY_WHELP_DEATH = 3255376551U;
        static const AkUniqueID PLAY_WHELP_HIT = 1421537766U;
        static const AkUniqueID PLAY_ZIGGURAT_ATTACK = 3841714128U;
        static const AkUniqueID PLAY_ZIGGURAT_DEAD = 2424990814U;
        static const AkUniqueID PLAY_ZIGGURAT_HIT = 1241744387U;
        static const AkUniqueID STOP_AMB_BONEYARD = 943615725U;
        static const AkUniqueID STOP_AMB_FINALBOSS = 1705183986U;
        static const AkUniqueID STOP_AMB_GLACIER = 1110089086U;
        static const AkUniqueID STOP_AMB_QUAGMIRE = 2088505380U;
        static const AkUniqueID STOP_GLACIAR_MOUNTAIN_SLIDE = 921935701U;
        static const AkUniqueID STOP_GLACIAR_MOUNTAINS_DESTRUCTION = 1913884299U;
        static const AkUniqueID STOP_MUX_BONEYARD = 2145900123U;
        static const AkUniqueID STOP_MUX_CREDIT = 2250919938U;
        static const AkUniqueID STOP_MUX_GLACIER = 1945283492U;
        static const AkUniqueID STOP_MUX_GLACIER_PROTOTYPE = 1213407967U;
        static const AkUniqueID STOP_MUX_LOSESCREEN = 2004671618U;
        static const AkUniqueID STOP_MUX_MENU = 826608728U;
        static const AkUniqueID STOP_MUX_QUAGMIRE = 1531223978U;
        static const AkUniqueID STOP_MUX_RAPTURE = 4228976348U;
        static const AkUniqueID STOP_TILE_SFX = 1999243856U;
        static const AkUniqueID STOP_UDEUKEDEFRUKE_SPLIT = 275449248U;
    } // namespace EVENTS

    namespace DIALOGUE_EVENTS
    {
        static const AkUniqueID VO_HIT_PLAYER = 1438980230U;
    } // namespace DIALOGUE_EVENTS

    namespace STATES
    {
        namespace MAINMUSIC_INTENTION
        {
            static const AkUniqueID GROUP = 2603289790U;

            namespace STATE
            {
                static const AkUniqueID HIGH = 3550808449U;
                static const AkUniqueID LOW = 545371365U;
                static const AkUniqueID MID = 1182670505U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MAINMUSIC_INTENTION

        namespace TURN
        {
            static const AkUniqueID GROUP = 3137665780U;

            namespace STATE
            {
                static const AkUniqueID ENEMY = 2299321487U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PLAYER = 1069431850U;
            } // namespace STATE
        } // namespace TURN

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MUSIC_INTENTION
        {
            static const AkUniqueID GROUP = 1129465863U;

            namespace SWITCH
            {
                static const AkUniqueID HIGH = 3550808449U;
                static const AkUniqueID LOW = 545371365U;
                static const AkUniqueID MID = 1182670505U;
            } // namespace SWITCH
        } // namespace MUSIC_INTENTION

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AMB_MASTER = 3073528060U;
        static const AkUniqueID BOARD_NUMBER = 2645207137U;
        static const AkUniqueID MUX_MASTER = 2095600934U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
        static const AkUniqueID UI_MASTER = 3075009468U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID ROBORAPTURE_SOUNDBANK = 1866478534U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID ENEMY = 2299321487U;
        static const AkUniqueID GENERAL_SFX = 322129659U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUX = 712897217U;
        static const AkUniqueID NEOSATAN = 2552676470U;
        static const AkUniqueID PLAYER = 1069431850U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID ENEMY_RVRB = 2191912134U;
        static const AkUniqueID MUX_REV = 4169781811U;
        static const AkUniqueID NEOSATAN_RVRB = 4139253385U;
        static const AkUniqueID PLAYER_RVRB = 837430453U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
