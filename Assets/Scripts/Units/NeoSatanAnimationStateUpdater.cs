﻿//-----------------------------------------------------------------------
// <copyright file="NeoSatanAnimationStateUpdater.cs" company="VFS">
// Copyright (c) VFS. All rights reserved.
// </copyright>
// <author>Angelica Mendez</author>
//-----------------------------------------------------------------------
namespace Edu.Vfs.RoboRapture.Units
{
    using Edu.Vfs.RoboRapture.Units.Actions;
    using UnityEngine;

    public class NeoSatanAnimationStateUpdater : MonoBehaviour
    {
        protected string idle = "Idle";

        protected string dead = "Dead";

        protected string hit = "Hit";

        protected string attack = "Attack";

        private string special = "Special";

        [SerializeField]
        protected Animator animator;

        [SerializeField]
        private ActionsHandler attackAction;

        public void Attack(Health health)
        {
            if (!health.HasIncreasedHealth)
            {
                this.animator.SetTrigger(this.hit);
            }
        }

        public void PlayAttackAnimation(int action)
        {
            if (action == 1)
            {
                this.animator.SetTrigger(this.attack);
            }
            else if (action == 2 || action == 3)
            {
                this.animator.SetTrigger(this.special);
            }
        }

        /// <summary>
        /// Method called from an event when attack animation ends.
        /// </summary>
        public void AttackEnded()
        {
            this.attackAction.Execute();
        }
    }
}