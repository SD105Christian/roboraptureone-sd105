﻿

/*
*      @Copyright: (c) 2019 All Rights Reserved
*      @Company: VFS ROBORAPTURE
*      @Contact: maquinodesign@gmail.com | pg15miguel@vfs.com
*      @Author: Carlos Miguel Aquino
*/

using System.Collections;
using System.Collections.Generic;
using Edu.Vfs.RoboRapture.DataTypes;
using Edu.Vfs.RoboRapture.EffectsSystem;
using Edu.Vfs.RoboRapture.Environment;
using Edu.Vfs.RoboRapture.Scriptables;
using Edu.Vfs.RoboRapture.SpawnSystem;
using Edu.Vfs.RoboRapture.TurnSystem;
using Edu.Vfs.RoboRapture.Units;
using UnityEngine;

///<summary>
///-summary of script here-
///</summary>
public class TesterASwell : MonoBehaviour
{
    // public ParticleSystem raptureCloud;
    public MeshRenderer Sphere;
    public EntityTurnManager etm;

    private MaterialPropertyBlock block;
    [SerializeField]
    private float bgalpha = 0;

    private int bgalphaPropID;

    [SerializeField]
    private float speed;

    private void Start()
    {
        etm.OnTurnEnd += BossSequence;

        block = new MaterialPropertyBlock();
        bgalphaPropID = Shader.PropertyToID("_TransitionAlpha");

        Sphere.SetPropertyBlock(block);

        block.SetFloat(bgalphaPropID, 0);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.BackQuote))
        {
        }
    }

    private void BossSequence(TurnEntities entities)
    {
        StopAllCoroutines();

        if(entities == TurnEntities.ENEMY)
        {
            // EffectsPostProcessingController.Instance.ApplyEffect(EffectType.VignetteFocus, 1, 0.5f, 0.4f);
            // EffectsPostProcessingController.Instance.ApplyEffect(EffectType.ChromaticAbberationFlash, 1, 0.5f, 0.8f);
            StartCoroutine(StartTransition(1));
        }

        if(entities == TurnEntities.PLAYER)
        {
            StartCoroutine(StartTransition(0));
        }
    }

    private IEnumerator StartTransition(int dest)
    {
        float t = 0f;
        
        while (bgalpha != dest)
        {
            t += Time.deltaTime / speed;
            bgalpha = AbsoluteLerp(bgalpha, (float)dest, t);
            block.SetFloat(bgalphaPropID, (float)bgalpha);
            Sphere.SetPropertyBlock(block);
            yield return null;
        }
    }

    private float AbsoluteLerp(float initialTarget, float target , float time)
    {
        return Mathf.Lerp(initialTarget, target, Mathf.SmoothStep(0.0f, 1.0f, time));
    }
}
